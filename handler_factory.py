from core.upload.exceptions import NoSuchHandler


class HandlerFactory:
    def __init__(self):
        self.registry = {}

    def register(self, handler):
        self.registry[handler.name] = handler

    def get_handler(self, name):
        try:
            return self.registry[name]
        except KeyError:
            raise NoSuchHandler('Invalid handler, {}'.format(name))
