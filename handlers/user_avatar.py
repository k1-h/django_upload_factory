from core.upload.file_types.image import ImageUpload
from core.upload.exceptions import ValidationError


class UserAvatar(ImageUpload):
    name = 'USER_AVATAR'
    upload_path = 'users/avatar/'

    def validate_upload(self):
        super(UserAvatar, self).validate_upload()

        if 'user_id' not in self.options:
            raise ValidationError(
                'User ID must be specified in options. Options: '.format(self.options)
            )

    def get_filename(self):
        return '{}_avatar.{}'.format(self.options['user_id'], self.extension)
