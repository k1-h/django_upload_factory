from core.upload import handler_factory
from core.upload.handlers.product_image import ProductImage
from core.upload.handlers.user_avatar import UserAvatar
from core.upload.handlers.user_profile_image import UserProfileImage
from core.upload.handlers.user_profile_video import UserProfileVideo


def register_handlers():
    handler_factory.register(ProductImage)
    handler_factory.register(UserAvatar)
    handler_factory.register(UserProfileImage)
    handler_factory.register(UserProfileVideo)
