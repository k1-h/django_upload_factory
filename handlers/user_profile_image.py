from uuid import uuid4

from core.upload.file_types.image import ImageUpload


class UserProfileImage(ImageUpload):
    name = 'USER_PROFILE_IMAGE'
    upload_path = 'users/images/'

    def get_filename(self):
        return '{}.{}'.format(uuid4().hex, self.extension)
