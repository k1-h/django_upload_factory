from core.upload.file_types.video import VideoUpload


class UserProfileVideo(VideoUpload):
    name = 'USER_PROFILE_VIDEO'
    upload_path = 'users/videos/'
