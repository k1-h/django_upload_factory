from core.upload.file_types.image import ImageUpload
from core.upload.exceptions import ValidationError


class ProductImage(ImageUpload):
    name = 'PRODUCT_IMAGE'
    upload_path = 'products/images/'

    def validate_upload(self):
        super(ProductImage, self).validate_upload()

        if 'product_id' not in self.options:
            raise ValidationError(
                'Product ID must be specified in options. Options: '.format(self.options)
            )

        if 'image_name' not in self.options:
            raise ValidationError(
                'Image name must be specified in options. Options: '.format(self.options)
            )

    def get_filename(self):
        return '{}_{}.{}'.format(
            self.options['product_id'],
            self.options['image_name'],
            self.extension,
        )
