from core.upload.handler_factory import HandlerFactory

handler_factory = None


def init():
    global handler_factory
    handler_factory = HandlerFactory()

    from core.upload.handlers import register_handlers
    register_handlers()


init()
