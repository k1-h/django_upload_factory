import abc
import os

import magic
from django.conf import settings

from core.upload.exceptions import ValidationError


class Upload:
    __metaclass__ = abc.ABCMeta

    upload_path = ''
    valid_mime_types = tuple()
    max_filesize = 10 * 1024 * 1024  # 10MB

    def __init__(self, file, options=None):
        self.file = file
        self.upload_path = os.path.join(settings.MEDIA_ROOT, self.upload_path)

        if not os.path.exists(self.upload_path):
            os.makedirs(self.upload_path)

        self.options = options if options is not None else {}

        self.validate_upload()
        self.prepare_file()
        self.save()

    def get_filename(self):
        """
        Each handler should implement this
        use md5sum for filenames?
        """
        raise NotImplementedError

    def validate_upload(self):
        """
        Make sure content type is valid, virus check, quota check, etc
        """
        mime = magic.from_buffer(self.file.read(1024), mime=True)
        # self.file.seek(0)       # return to beginning of file
        if mime not in self.valid_mime_types:
            raise ValidationError('Invalid file type, mime: {}'.format(mime))

        if self.file.size > self.max_filesize:
            raise ValidationError('File is too large, size: {}'.format(self.file.size))

    def prepare_file(self):
        """
        Lower file size, change image height and width, convert format, etc
        """
        pass

    def save(self):
        with open(os.path.join(self.upload_path, self.get_filename()), 'wb+') as upload:
            print os.path.join(self.upload_path, self.get_filename())
            for chunk in self.file.chunks():
                upload.write(chunk)
