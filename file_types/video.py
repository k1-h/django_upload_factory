import abc

from core.upload.upload import Upload


class VideoUpload(Upload):
    __metaclass__ = abc.ABCMeta

    max_filesize = 40 * 1024 * 1024  # 40MB
