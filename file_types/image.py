import abc
import logging
from tempfile import TemporaryFile

from PIL import Image

from django.core.files import File
from core.upload.upload import Upload
from core.upload.exceptions import ValidationError


logger = logging.getLogger(__name__)


class ImageUpload(Upload):
    __metaclass__ = abc.ABCMeta

    max_filesize = 1 * 1024 * 1024  # 1MB
    max_width = 1000
    max_height = 1000

    extension = 'jpg'
    convert_format = 'JPEG'
    valid_mime_types = (
        'image/jpeg',
        'image/png',
    )

    def prepare_file(self):
        """
        Enforce image width and height
        Enforce file type
        Enfore file size
        """
        processed_image = TemporaryFile()
        try:
            with Image.open(self.file) as image:
                image = image.resize((self.max_width, self.max_height))  # this might cause some unclosed wandering files, FIXME
                image.save(processed_image, self.convert_format)
        except IOError:
            raise ValidationError('Invalid image format')
        except Image.DecompressionBombWarning:
            logger.critical('Potential Decompression Bomb attack to server')
            raise ValidationError('Invalid image format')

        self.file = File(processed_image)  # Replacing file instance might not be good idea, got any better?
